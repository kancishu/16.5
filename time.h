#include <ctime>

int currentDate()
{
	struct tm newtime;
	std::time_t t = std::time(nullptr);
	localtime_s(&newtime, &t);
	return newtime.tm_mday;
}

